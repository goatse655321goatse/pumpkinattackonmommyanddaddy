<html>
  <head>
    <link rel='stylesheet' href='style.css' />
  </head>
  </head>
  <body>

<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}



// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
    <div class="contentAll">
      <form action="" method="POST">
        <input name="fio" id="name" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" placeholder="ФИО" />

        <label><input type="email" id="mail" name="email" placeholder="E-mail" 
        <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" /></label>

        <select name="date" id="data" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>">
          
          <?php for($i = 1900; $i < 2020; $i++) { ?>
            <option value="<?php print $i; ?>" <?php if($values['date'] == "$i"){ echo 'selected="selected"'; }?>> <?= $i; ?> </option>
          <?php } ?>
		    </select>
        <label for="sex" ><p>Пол: </p></label>
        <div>
          <input type="radio"  id="dsbld" name="sex" value="dsbld" checked disabled />
          <label for="Male">Не знаю</label>
          <input type="radio"  id="Male" name="sex" value="M" <?php if($values['sex'] == "M"){ echo 'checked="checked"'; }?> />
          <label for="Male">Мужской</label>
          <input type="radio" id="Female" name="sex" value="F" <?php if($values['sex'] == "F"){ echo 'checked="checked"'; }?> />
          <label for="Female">Женский</label>
        </div>

        <label for="limbs"><p>Количество конечностей: </p></label>
        <div>
          <input type="radio" id="Zero" name="limbs" value="0" checked disabled >
          <label for="Zero">Ни одной</label>

          <input type="radio" id="One" name="limbs" value="4" <?php if ($errors['limbs']) {print 'class="error"';} ?> <?php if($values['limbs'] == "4"){ echo 'checked="checked"'; }?>>
          <label for="One">Четыре</label>

          <input type="radio" id="Two" name="limbs" value="2" <?php if ($errors['limbs']) {print 'class="error"';} ?> <?php if($values['limbs'] == "2"){ echo 'checked="checked"'; }?> >
          <label for="Two">Две</label>

          <input type="radio" id="Three" name="limbs" value="6" <?php if ($errors['limbs']) {print 'class="error"';} ?> <?php if($values['limbs'] == "6"){ echo 'checked="checked"'; }?>>
          <label for="Three">Шесть</label>
        </div>
        <br>

        <!--<div  id="Abilit">
          <select  id="Abilit" name="abilities[]" multiple
          <?php if ($errors['abilities']) {print 'class="error"';} ?> value="<?php print $values['abilities']; ?>">
            <option value="0" disabled>Сверхспособность</option>
            <option value="Immortality" <?php if($values['one'] == "1"){ echo 'selected="selected"'; }?>>Бессмертие</option>
            <option value="Through_the_walls" <?php if($values['two'] == "1"){ echo 'selected="selected"'; }?>>Прохождение сквозь стены</option>
            <option value="Levitation" <?php if($values['three'] == "1"){ echo 'selected="selected"'; }?>>Левитация</option>
          </select>
        </div>-->


        <select id="Abilit" name="abilities[]" multiple <?php if ($errors['abilities']) {print 'class="error"';} ?>>
<?php 
foreach ($abilities as $key => $value) {
  $selected = empty($values['abilities'][$key]) ? '' : 'selected="selected"';
  printf('<option value="%s"%s>%s</option>', $key, $selected, $value);
}     
?>
        </select><br />    

        <div><textarea name="biography" id="Biograph" rows="4" cols="50" placeholder="Биография" <?php if ($errors['biography']) {print 'class="error"';} ?> value="<?php print $values['biography']; ?>"><?php print $values['biography']; ?></textarea></div>

        <label for="Agree">На всё согласен: </label>
        <input type="checkbox" id="Agree" name="agree" value="Agree" <?php if ($errors['agree']) {print 'class="error"';} ?> <?php if($values['agree']){ echo 'checked="checked"'; }?>">

        <div><input type="submit" value="Отправить" id="btnsubmit"/></div>
      </form>
    </div
  </body>
</html>
