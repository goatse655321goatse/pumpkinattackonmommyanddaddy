<?php
/**
 * Реализовать проверку заполнения обязательных полей формы в предыдущей
 * с использованием Cookies, а также заполнение формы по умолчанию ранее
 * введенными значениями.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');
$abilities = ['Immortality' => 'Бессмертие', 'Througn_the_walls'=> 'Леха', 'Levitation' => 'Левитация'];
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';
  }
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['agree'] = !empty($_COOKIE['agree_error']);

  if ($errors['fio']) {
    setcookie('fio_error', '', 100000);
    if($_COOKIE['fio_error'] == '1') {
      $messages[] = '<div class="error">Заполните имя.</div>';
    } else {
      $messages[] = '<div class="error">Проверьте символы.</div>';
    }
  }

  if ($errors['email']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    if($_COOKIE['email_error'] == '1') {
      $messages[] = '<div class="error">Заполните адрес электронной почты.</div>';
    } else {
      $messages[] = '<div class="error">Проверьте символы.</div>';
    }
  }

  if ($errors['date']) {
    //Удаляем куку, указывая время устаревания в прошлом.
    setcookie('date_error', '', 100000);
    //Выводим сообщение.
    $messages[] = '<div class="error">Выберите год.</div>';
  }

  if ($errors['sex']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sex_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберите пол.</div>';
  }

  if ($errors['abilities']) {
    setcookie('abilities_error', '', 100000);
    $messages[] = '<div class="error">Выберите способность.</div>';
  }

  if ($errors['limbs']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('limbs_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Выберите количество конечностей.</div>';
  }

   if ($errors['biography']) {
      setcookie('biography_error', '', 100000);
      $messages[] = '<div class="error">Расскажите о себе.</div>';
  }

   if ($errors['agree']) {
      setcookie('agree_error', '', 100000);
      $messages[] = '<div class="error">Согласитесь, иначе форма не отправится.</div>';
  }

  $values = array();
  $values['fio'] = (empty($_COOKIE['fio_value']) || !preg_match('/^[а-яА-Я ]+$/u', $_COOKIE['fio_value'])) ? '' : $_COOKIE['fio_value'];
  $values['email'] = (empty($_COOKIE['email_value']) || !preg_match('/^.+@.+\..+$/u', $_COOKIE['email_value'])) ? '' : $_COOKIE['email_value'];
  $values['date'] = (empty($_COOKIE['date_value'])) ? "1900" : $_COOKIE['date_value'];
  //$values['abilities'] = (empty($_COOKIE['abilities'])) ? '' : $_COOKIE['abilities'];
  /*$values['one'] = (empty($_COOKIE['one'])) ? '' : $_COOKIE['one'];
  $values['two'] = (empty($_COOKIE['two'])) ? '' : $_COOKIE['two'];
  $values['three'] = (empty($_COOKIE['three'])) ? '' : $_COOKIE['three'];*/
  $values['sex'] = (empty($_COOKIE['sex_value'])) ? '' : $_COOKIE['sex_value'];
  $values['limbs'] = (empty($_COOKIE['limbs_value'])) ? '' : $_COOKIE['limbs_value'];
  $values['biography'] = (empty($_COOKIE['biography_value'])) ? '' : $_COOKIE['biography_value'];
  $values['agree'] = (empty($_COOKIE['agree_value'])) ? '' : $_COOKIE['agree_value'];
  // TODO: аналогично все поля.

  $values['abilities'] = [];
  if (!empty($_COOKIE['abilities_value'])) {
    $abilities_cookies = json_decode($_COOKIE['abilities_value']);
    if(is_array($abilities_cookies)) {
      foreach($abilities_cookies as $ability) {
        if (!empty($abilities[$ability])) {
          $values['abilities'][$ability] = $ability;
        }
      }
    }
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (empty($_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else if (!preg_match('/^[а-яА-Я ]+$/u', $_POST['fio'])) {
    setcookie('fio_error', '2', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['email'])) {
    // Выдаем куку на день с флажком об ошибке в поле email.
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else if (!preg_match('/^.+@.+\..+$/u', $_POST['email'])) {
    setcookie('email_error', '2', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['date'])) {
    // Выдаем куку на день с флажком об ошибке в поле date.
    setcookie('date_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['abilities'])) {
    // Выдаем куку на день с флажком об ошибке в поле abilities.
    setcookie('abilities_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    foreach($_POST['abilities'] as $ability) {
      if (empty($abilities[$ability])) {
        print('Нельзя');
        exit();
      }
    }
    setcookie('abilities_value', json_encode($_POST['abilities']), time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['sex'])) {
    // Выдаем куку на день с флажком об ошибке в поле abilities.
    setcookie('sex_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['limbs'])) {
    // Выдаем куку на день с флажком об ошибке в поле abilities.
    setcookie('limbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['biography'])) {
    // Выдаем куку на день с флажком об ошибке в поле biography.
    setcookie('biography_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
  }

  if (empty($_POST['agree'])) {
    // Выдаем куку на день с флажком об ошибке в поле agree.
    setcookie('agree_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  } else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('agree_value', $_POST['agree'], time() + 30 * 24 * 60 * 60);
  }



  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }

  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('agree_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
  }

  // Сохранение в XML-документ.
  // ...
  $user = 'u20366';
  $pass = '5918475';
  $db = new PDO('mysql:host=localhost;dbname=u20366', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

  try {
    $stmt = $db->prepare("INSERT INTO application (name, email, sex, biography, birth_year, limbs, ability, agree) VALUES (:fio, :email, :sex, :biography, :date, :limbs, :ability, :agree)");
    $fio = $_POST['fio'];
    $stmt->bindParam(':fio', $fio, PDO::PARAM_STR);
    $email = $_POST['email'];
    $stmt->bindParam(':email', $email, PDO::PARAM_STR);
    $sex = $_POST['sex'];
    $stmt->bindParam(':sex', $sex, PDO::PARAM_STR);
    $biography = $_POST['biography'];
    $stmt->bindParam(':biography', $biography, PDO::PARAM_STR);
    $date = $_POST['date'];
    $stmt->bindParam(':date', $date, PDO::PARAM_STR);
    $limbs = $_POST['limbs'];
    $stmt->bindParam(':limbs', $limbs, PDO::PARAM_INT);
    $ability = $_POST['abilities'];
    $ability = implode(' ', $_POST['abilities']);
    $stmt->bindParam(':ability', $ability, PDO::PARAM_STR);
    $agree = $_POST['agree'];
    $stmt->bindParam(':agree', $agree, PDO::PARAM_STR);
    $stmt->execute();
  }
  catch(PDOException $e) {
    print ('Error : ' . $e->getMessage());
    exit();
  }

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: index.php');
}
